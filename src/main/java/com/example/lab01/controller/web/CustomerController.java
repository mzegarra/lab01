package com.example.lab01.controller.web;

import com.example.lab01.controller.web.dto.CustomerWebDto;
import com.example.lab01.controller.web.dto.CustomersWebDto;
import com.example.lab01.core.domain.Customer;
import com.example.lab01.service.CustomerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public HttpEntity<CustomersWebDto> getList() {

        List<Customer> customers;
        try{
            customers = customerService.getList();
            CustomersWebDto customersWebDto = new CustomersWebDto(customers);
            return new ResponseEntity<>(customersWebDto, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @GetMapping("/{id}")
    public HttpEntity<CustomerWebDto> getById(@PathVariable("id") int id) {

        Customer customer = customerService.getById(id);

        if(customer!=null){
            CustomerWebDto customerWebDto = new CustomerWebDto();
            customerWebDto.setCustomer(customer);
            return new ResponseEntity<>(customerWebDto, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping
    public void create(@RequestBody Customer customer) {

        StringUtils.capitalize(customer.getNombre());

        customerService.save(customer);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable(name = "id") int id,
                       @RequestBody Customer customer) {

        customerService.update(customer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") int id) {
        Customer customer =new Customer();
        customer.setCustomerId(id);
        customerService.delete(customer);
    }

}