package com.example.lab01.repository;

import com.example.lab01.core.domain.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    @Override
    public Customer getById(int id) {

        Customer customer1 = new Customer();
        customer1.setCustomerId(1);
        customer1.setNombre("Manuel");
        return customer1;
    }

    @Override
    public List<Customer> getList() {
        Customer customer1 = new Customer();
        customer1.setCustomerId(1);
        customer1.setNombre("Manuel");

        Customer customer2 = new Customer();
        customer2.setCustomerId(2);
        customer2.setNombre("Juan");
        customer2.setPaterno("Logan");

        return  Arrays.asList(customer1,customer2);

    }

    private static final Logger log = LoggerFactory.getLogger(CustomerRepositoryImpl.class);


    @Override
    public void save(Customer customer) {
        log.debug("save");
    }

    @Override
    public void update(Customer customer) {
        log.info("update {}",customer.getCustomerId());
    }

    @Override
    public void delete(Customer customer) {
        log.warn("delete{}",customer.getCustomerId());
    }





}
